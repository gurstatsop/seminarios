---
title: "Como financiar sua pesquisa pelo Google Summer of Code"
author: "Raniere Silva"
date: "20 de Fevereiro de 2020"
output: 
  slidy_presentation:
    css: styles.css
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Objetivos

- Explicar sobre o Google Summer of Code
- Explicar participação de alunos
- Explicar participação de mentores

## Google Summer of Code (GSoC)

Um programa global focado em aumentar o número de estudantes que contribuem com projetos de **software aberto**.

## Software Aberto

Definido pela [Open Source Initiative](https://opensource.org/) como

1. Free Redistribution
2. Source Code
3. Derived Works
4. Integrity of The Author's Source Code
5. No Discrimination Against Persons or Groups
6. No Discrimination Against Fields of Endeavor
7. Distribution of License
8. License Must Not Be Specific to a Product
9. License Must Not Restrict Other Software
10. License Must Be Technology-Neutral

## Software Aberto em 2002

![Microsoft raps open-source approach](microsoft-raps-open-source.png)

## Software Aberto em 2016

![Microsoft Steps Up Its Commitment to Open Source](microsoft-join-linux.png)

## Como Funciona

Estudantes trabalham para um projeto de software aberto por três meses durante as "férias" escolares. Google remunera os alunos pelo trabalho.

## Remuneração

US$3600

- Após primeira avaliação: 30% pagos em 1 de Julho
- Após segunda avaliação: 30% pagos em 29 de Julho
- Após terceira avaliação: 40% pagos em 5 de Setembro

Maiores detalhes em https://developers.google.com/open-source/gsoc/help/student-stipends

## Compração da Remuneração

Programa Valor da Bolsa (ano) Dedicação
-------- -------------------- ------------------------
PIBIC    R$4.800,00           20h semanais por 1 ano
GSoC     R$17.280,00          40h semanais por 3 meses
CAPES    R$18.000,00          40h semanais por 2 anos
FAPEMIG  R$18.000,00          40h semanais por 2 anos
FAPESP   R$26,000,00          40h semanais por 2 anos

Utilizado US\$1,00 = R\$4,80.

## Calendário

- 20 de Fevereiro: Projeto de software aberto anunciados
- 16 de Março: Início da submissão de projetos
- 31 de Março: Encerramento da submissão de projetos
- 27 de Abril: Anúncio dos estudantes selecionados
- 18 de Maio: Início das atividades no projeto
- 10 de Agosto: Encerramento das atividades no projeto

## Organizações de Software Aberto (1/3)

- [R Project for Statistical Computing](https://summerofcode.withgoogle.com/organizations/6259938323595264/)
- [NumFOCUS](https://summerofcode.withgoogle.com/organizations/4727917315096576/)
- [GeomScale](https://summerofcode.withgoogle.com/organizations/5673184117915648/)

## Organizações de Software Aberto (2/3)

- [National Resource for Network Biology (NRNB)](https://summerofcode.withgoogle.com/organizations/5848353922875392/)
- [Canadian Centre for Computational Genomics](https://summerofcode.withgoogle.com/organizations/6542198541123584/)
- [Open Genome Informatics](https://summerofcode.withgoogle.com/organizations/4980644230201344/)
- [PEcAn Project](https://summerofcode.withgoogle.com/organizations/6396068419338240/)

## Organizações de Software Aberto (3/3)

- [Debian](https://summerofcode.withgoogle.com/organizations/5466729234300928/)
- [Fedora Project](https://summerofcode.withgoogle.com/organizations/5627797017460736/)

## Benefícios para Alunos

- US$3600
- Destaque no CV (entrevistadores sempre perguntam sobre a experiência no GSoC)
- Destaque no seu portifólio online: GitHub, GitLab, ...
- Experiência prática de trabalho em grupo onde é utilizado boas práticas de engenharia de software
- Contatos para pedido de carta de referência

## Requisitos Necessários para Participar

- Ter mais de 18 anos
- Poder compravar ser um estudante: técnico, graduação, mestrado ou doutorado.
- **Não** é redidente em um país na lista de "inimigos" dos Estados Unidos
- **Não** ter participado como aluno no GSoC duas ou mais vezes anteriormente

## Requisitos Desejáveis para Participar

- Técnicos
  - Conhecimento de lógica de programação
  - Experiência com a linguagem a ser utilizada no projeto
  - Familiaridade com controle de versão
- Gerenciais
  - Planejamento para os três meses de projeto
  - Responsável com horários
- Pessoais
  - Compreensível (por exemplo, negociar horário de reuniões)
  - Comunicativo (por exemplo, pedir ajuda quando precisar)
  - Amigável (por exemplo, **não** fazer piadas ofensivas)

## Testemunho de Alunos

> ![Hannah](story645.jpeg)
> "the story of my GSOC is I stuck around, became a core dev, got really involved on the community side of things, and am now funded under CZI to theorize Matplotlib's architecture for my dissertation" (Hannah, aluna de doutorado na The Graduate Center
/City College of NY)

## Benefícios para Mentores

- Delegar parte do projeto para o aluno
- Ganho de experiência como mentor
- Destaque no CV (entrevistadores sempre perguntam sobre a experiência no GSoC)

<div class='left' style='float:left;width:48%'>
![T-shirt. Fonte: https://twitter.com/ruehsen/status/1186686029459316736](mentor-tshirt.jpeg)
</div>
<div class='right' style='float:right;width:48%'>
![Mesa de chocolate. Fonte: https://twitter.com/catallman/status/1068922974818885632](chocolate.jpeg)
</div>

## Testemunho de Mentores

> ![Yo Yehudi](yo.jpeg)
> "Students from the last three years have become long-term contributors and mentors, and one has joined the team as full-time staff. Other students have joined us to work as summer interns in later years. Many student contributions are high-visibility and high-impact, and are now in production." (Yo Yehudi, engenheira de software no projeto InterMine, University of Cambridge)

> ![Henry Senyondo](henry.jpg)
> "Through Google Summer of Code, the Data Retriever has improved the usability of the R package by synchronizing R API with the Python API and improve usability of R package hence maximizing the functionalities of the API." (Henry Senyondo, desenvolvedor de software no Weecology, University of Florida)

## Benefícios para Organizações

- Divulgação "gratuita"
- US$500 por projeto/aluno
- 480 horas de trabalho por aluno
- Conhecer possíveis contribuidores
- Conhecer possíveis membros

## Google Summer of Code

É uma grande oportunidade para todos
que ajuda abrir novas oportunidades.

![Logo do Google Summer of Code](GSoC-logo-horizontal.svg)

## Notas finais para alunos

Submissão de projetos encerra no dia 31 de Março.
Você ainda pode participar em 2020:
olhe as organizações,
leia os projetos sugeridos,
escreva e envie seu projeto.

## Notas finais para mentores

Começe a pensar em um projeto para você mentorar em 2021.
E envolva-se com as organizações.

## Patrocínio

![R Consortium](RConsortium_Vertical_Pantone.png)

## Grupo de Usuários de R em Ouro Preto

Apresentação será disponibilizada em https://gurstatsop.gitlab.io/

Outras atividades:

- Tidy Tuesday, Terças das 12:30 às 13:30
- Monitoria de R, Quintas das 12:30 às 13:30